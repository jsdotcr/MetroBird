﻿(function () {
  "use strict";

  var nav = WinJS.Navigation;
  var session = WinJS.Application.sessionState;
  var util = WinJS.Utilities;
  var heartbeatData = Windows.Storage.ApplicationData.current.roamingSettings.values['heartbeat'];

  WinJS.UI.Pages.define("/pages/hub.html", {
    processed: function (element) {
      return WinJS.Resources.processAll(element);
    },

    // This function is called whenever a user navigates to this page. It
    // populates the page elements with the app's data.
    ready: function (element, options) {
      if (typeof (heartbeatData) === 'string') {
        heartbeatData = JSON.parse(heartbeatData);
      }
      if (!heartbeatData || !heartbeatData.__sessionid) {
        Login.logout();
      }
      var hub = element.querySelector(".hub").winControl;
      hub.onheaderinvoked = function (args) {
        args.detail.section.onheaderinvoked(args);
      };
      hub.onloadingstatechanged = function (args) {
        if (args.srcElement === hub.element && args.detail.loadingState === "complete") {
          hub.onloadingstatechanged = null;
        }
      }
      document.getElementById('appbar').winControl.disabled = false;

      // TODO: Initialize the page here.
    },
    /*
      section3DataSource: section3Items.dataSource,

      section3HeaderNavigate: util.markSupportedForProcessing(function (args) {
          nav.navigate("/pages/section/section.html", { title: args.detail.section.header, groupKey: section3Group.key });
      }),

      section3ItemNavigate: util.markSupportedForProcessing(function (args) {
          var item = Data.getItemReference(section3Items.getAt(args.detail.itemIndex));
          nav.navigate("/pages/item/item.html", { item: item });
      }),
    */

    unload: function () {
      // TODO: Respond to navigations away from this page.
    },

    updateLayout: function (element) {
      /// <param name="element" domElement="true" />

      // TODO: Respond to changes in layout.
    }
  });
})();