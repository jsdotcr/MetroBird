﻿(function () {
    "use strict";
    var dataList;
    var roamingSettings = Windows.Storage.ApplicationData.current.roamingSettings;

    var ControlConstructor = WinJS.UI.Pages.define("/pages/search.html", {
        // This function is called after the page control contents 
        // have been loaded, controls have been activated, and 
        // the resulting elements have been parented to the DOM. 
        ready: function (element, options) {
          options = options || {};
          var radarSearch = document.getElementById('radar').winControl;
          var locationCoords = JSON.parse(roamingSettings.values['location'] || "{}");
          if (locationCoords.latitude || locationCoords.longitude) {
            console.log('WTF?!');
          }
          WinJS.xhr({
            url: 'https://api.planetromeo.com/v3.2/users/search?__apikey=BlueBirdApiKeyForDevelopment2014&__sessionid=' + Login.getSessionId(),
            data: {
              sorting: 'NEARBY_ASC',
              latitude: locationCoords.latitude || 52.5663364,
              longitude: locationCoords.longitude || 13.5557645
            },
            responseType: "json"
          }).done(function (xhr) {
            var r = JSON.parse(xhr.response);
            if (r.data) {
              dataList = new WinJS.Binding.List(r.data);
              var filteredList = dataList.createFiltered(function (item) {
                return item.preview_pic && item.onlinestatus !== 'OFFLINE';
              });
              radarSearch.itemDataSource = filteredList.dataSource;
            }
          }, function (xhr) {
            var r = JSON.parse(xhr.response);
            var msg = new Windows.UI.Popups.MessageDialog(r.error_message);
            msg.commands.append(
                new Windows.UI.Popups.UICommand("Close"), function () {
                  Login.logout();
                });
            msg.cancelCommandIndex = 0;
            msg.showAsync();
          }, function (xhr) {
            console.log(xhr);
          });

          
        }
    });

    // The following lines expose this control constructor as a global. 
    // This lets you use the control as a declarative control inside the 
    // data-win-control attribute. 

    WinJS.Namespace.define("HubApps_SectionControls", {
      SearchControl: ControlConstructor,
      radarSearch: dataList
    });
})();