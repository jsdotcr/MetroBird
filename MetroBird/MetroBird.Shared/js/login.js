﻿(function () {
  "use strict";

  var util = WinJS.Utilities;
  var nav = WinJS.Navigation;
  var heartbeatWorker = null;
  var roamingSettings = Windows.Storage.ApplicationData.current.roamingSettings;

  WinJS.UI.Pages.define("/pages/login.html", {
    processed: function (element) {
      return WinJS.Resources.processAll(element);
    }
  });

  var vault = new Windows.Security.Credentials.PasswordVault(),
    vaultResource = 'PlanetRomeo',
    credential = null;
  try {
    credential = vault.findAllByResource(vaultResource)[0];
    credential = vault.retrieve(vaultResource, credential.userName);
  } catch (e) { }


  function _onLoginSuccess(username, password) {
    if (!credential) {
      vault.add(new Windows.Security.Credentials.PasswordCredential("PlanetRomeo", username, password));
    }
    Geo.getLocation();
    return nav.navigate(nav.location || Application.navigator.home);
  }

  function isNeeded() {
    return !credential || !credential.userName;
  }
  function onLoginSubmit(args) {
    args.preventDefault();
    executeLogin(args.srcElement.elements.username.value, args.srcElement.elements.password.value);

    return false;
  }
  function executeLogin(username, password) {
    if (credential){
      username = credential.userName;
      password = credential.password;
    }
    return WinJS.xhr({
      url: 'https://api.planetromeo.com/v3.2/auth/login?__apikey=BlueBirdApiKeyForDevelopment2014&username=' + username + '&password=' + password,
      responseType: "json"
    }).then(function (xhr) {
      var r = JSON.parse(xhr.response);
      if (r && r.__sessionid) {
        heartbeatWorker = new Worker('/js/heartbeat.js');
        heartbeatWorker.onmessage = function (e) {
          if (e.data.stillAlive === false) {
            var msg = new Windows.UI.Popups.MessageDialog(e.data.reason);
            msg.commands.append(
               new Windows.UI.Popups.UICommand("Close"));
            msg.cancelCommandIndex = 0;
            msg.showAsync();

            return logout();
          } else if (e.data.stillAlive) {
            _onLoginSuccess(username, password);
          }
        };
        heartbeatWorker.postMessage({
          data: xhr.response
        });
      } else {
        var msg = new Windows.UI.Popups.MessageDialog('Failed to parse response. That is usually terribly wrong.');
        msg.commands.append(
          new Windows.UI.Popups.UICommand("Close"), function () {
            return nav.navigate('/pages/login.html');
        });
        msg.cancelCommandIndex = 0;
        msg.showAsync();
      }
    }, function (xhr) {
      var r = JSON.parse(xhr.response);
      if (r.pr_error_code === "401") {
        var msg = new Windows.UI.Popups.MessageDialog(r.error_message);
        msg.commands.append(
           new Windows.UI.Popups.UICommand("Close"));
        msg.cancelCommandIndex = 0;
        msg.showAsync();
      }
    });
  }

  function logout() {
    stopHeartbeat();
    WinJS.xhr({
      url: 'https://api.planetromeo.com/v3.2/auth/logout?__apikey=BlueBirdApiKeyForDevelopment2014',
      responseType: "json"
    }).then(function (xhr) {
      return nav.navigate('/pages/login.html');
      // set the session
    }, function (xhr) {
      return nav.navigate('/pages/login.html'); // @TODO WTF?!
      var r = JSON.parse(xhr.response);
      var msg = new Windows.UI.Popups.MessageDialog(r.error_message);
      msg.commands.append(
         new Windows.UI.Popups.UICommand("Close"));
      msg.cancelCommandIndex = 0;
      return msg.showAsync().then(function(command) {
        if (command && command.id === 0) {
          return nav.navigate('/pages/login.html');
        }
      });
    });
  }

  function stopHeartbeat() {
    heartbeatWorker.postMessage({
      die: true
    });
  }
  function getSessionId() {
    var hVal = JSON.parse(Windows.Storage.ApplicationData.current.roamingSettings.values['heartbeat']);
    return hVal.__sessionid;
  }

  WinJS.Namespace.define("Login", {
    onLoginSubmit: util.markSupportedForProcessing(onLoginSubmit),
    isNeeded: isNeeded,
    logout: util.markSupportedForProcessing(logout),
    autoLogin: executeLogin,
    stopHeartbeat: stopHeartbeat,
    getSessionId: getSessionId
  });
})();
