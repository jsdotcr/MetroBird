﻿/// <reference group="Dedicated Worker" />
importScripts('//Microsoft.Phone.WinJS.2.1/js/base.js');

(function () { 
  "use strict"; 

  var roamingSettings = Windows.Storage.ApplicationData.current.roamingSettings;

  function beat() {
    WinJS.xhr({
      url: 'https://api.planetromeo.com/v3.2/auth/heartbeat?__apikey=BlueBirdApiKeyForDevelopment2014',
      responseType: "document"
    }).then(function onSuccess(xhr) {
      var r = JSON.parse(xhr.response);
      if (r && r.__sessionid) {
        console.log('heartbeat executed!');
        updateRoamingData();
      } else {
        heartbeatKiller('Invalid JSON returned by the API.', xhr.response);
      }
    }, function onError(xhr) {
      heartbeatKiller('Error status returned by API', xhr.response);
    });
  }

  function heartbeatKiller(reason, data) {
    clearInterval(beatIntervalId);
    delete roamingSettings.values['heartbeat'];
    postMessage({
      stillAlive: false,
      reason: reason,
      additionalData: data || null
    });
  }

  function updateRoamingData(data) {
    if (typeof (data) !== 'string') {
      data = JSON.stringify(data);
    }
    if (data /*&& roamingSettings.values['heartbeat'] !== data*/) {
      roamingSettings.values['heartbeat'] = data;
      postMessage({
        stillAlive: true,
        lastCheck: +new Date(),
        response: JSON.parse(data)
      });
    }
  }

  self.onmessage = function (event) {
    if (event.data) {
      updateRoamingData(event.data.data);
    } else if (event.data.die) {
      heartbeatKiller('Somebody from outside the Worked asked to kill the heartbeat');
    }
  }

  var beatIntervalId = setInterval(beat, 1000 * 60 * 5);
})(); 