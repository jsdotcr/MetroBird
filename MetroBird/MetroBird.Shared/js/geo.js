﻿(function () {
  "use strict";
  var roamingSettings = Windows.Storage.ApplicationData.current.roamingSettings;
  var loc = new Windows.Devices.Geolocation.Geolocator();

  function getLocation(){
    if (loc !== null) {
      loc.getGeopositionAsync().then(function onSuccess(pos) {
        roamingSettings.values['location'] = JSON.stringify({
          latitude: pos.coordinate.point.position.latitude,
          longitude: pos.coordinate.point.position.longitude,
          accuracy: pos.coordinate.accuracy
        });
      }, function onError(e) {
        console.log('Bad bad things happened here.', e);
        return false;
      });
    }
  }

  function startTracking() {
    console.log('Not implemented yet!');
    return false;
  }
  function stopTracking() {
    console.log('Not implemented yet!');
    return false;
  }

  WinJS.Namespace.define("Geo", {
    getLocation: getLocation,
    startTracking: startTracking,
    stopTracking: stopTracking
  });
})();
