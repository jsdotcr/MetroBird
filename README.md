# MetroBird #

### What is this repository for? ###

PlanetRomeo's missing App for Windows 8.1 (desktop, tablets and smartphones) codebase. This is actually just a working demo so try not to compare with the Android version, huh?


### Timeslots ###
Day | Hours | Beers |
--- | ----- | ----- |
Friday | 10am-7pm | 6 |
Saturday | 11am-8pm | 6 |
Sunday | 12am-8pm | 4 |


### Roadmap ###

Feature | Time spent on it | Is it done? | Remarks | 
------- | ---------------- | ----------- | ------- |
**Login** | 8 beers | ✔ | |
**Main Hub** | 5 beers | ✔ | |
**Radar search** | 3 beers | ✓✗ | Search API is an hell. |
**Profile** | 0 beers | ✘ | |
**Messages** | 0 beers | ✘ | |
**Desktop view** | 0 beers | ✘ | |


### How do I get set up? ###

The following things are required to run/debug/develop the app:

* Windows 8.1 with the latest updates;
* Visual Studio 2013, latest version; 
* Microsoft Developer license; 
* WP emulator or, even better, a developer-unlocked Windows Phone device with WP8.1 installed;
* Not blaming Microsoft for what they've done twelve-fucking-years ago.

### Who do I talk to? ###
* rocco.curcio@erasys.de